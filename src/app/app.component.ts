import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { NativeStorage } from '@ionic-native/native-storage';

import { AngularFireAuth } from 'angularfire2/auth';

import { ChatPage } from '../pages/chat/chat';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  rootPage:any 
    // = ChatPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    nativeStorage: NativeStorage,
    public events: Events,
    public menuCtrl: MenuController,
    private _afAuth: AngularFireAuth
  ) {

    platform.ready().then(() => {

      // checking if user is already logged in
      nativeStorage.getItem('user')
      .then(data => {
        //if logged in, send the user to homepage
        this.nav.push(HomePage);
        splashScreen.hide();
      }, (err) => {
        //if not, send the user to loginpage
        this.nav.push(LoginPage);
      })
      
      splashScreen.hide();
      statusBar.styleDefault();
    });
  }

  logout() {
    this.events.publish('user:logout');
    this.menuCtrl.close();
    this._afAuth.auth.signOut()
    .catch(err => alert('could not sign out: ' + JSON.stringify(err)));
  }
}

