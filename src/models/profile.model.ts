
export interface Profile{
    name: string,
    userId: string,
    picture: string,
}