export interface Listing {
    category: string;
    quantity: number;
    unit: string;
    priceMax: number;
    priceMin: number;
    user: string;
}