//Single message model

export interface Message {
    body: string,
    owner: string
}