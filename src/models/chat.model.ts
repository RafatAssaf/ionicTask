import { Message } from "./message.model";

export interface Chat {
    //contacts[0]: seller, contacts[1]: buyer
    contacts: string[],
    //messages record key
    messages: Message[]
}