import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';

import * as admin from 'firebase-admin';

import { AngularFireAuth } from 'angularfire2/auth';

import { HomePage } from '../home/home';

@IonicPage()

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  FB_APP_ID: number = 381144615660538;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: Facebook,
    public nativeStorage: NativeStorage,
    public _afAuth: AngularFireAuth,

  ) {
    this.fb.browserInit(this.FB_APP_ID, 'v2.8');
  }

  loginWithFacebook() {
    let premissions = new Array<string>();
    premissions = ['public_profile'];

    this.fb.login(premissions)
    .then(res => {
      let userId = res.authResponse.userID;
      let params = new Array<string>();
      
      // sign the user to firebase
      admin.auth().createCustomToken(res.authResponse.userID)
      .then((customToken) => {
        this._afAuth.auth.signInWithCustomToken(customToken)
        .then(() => alert('user logged in to firebase'))
        .catch(err => alert(JSON.stringify(err)))
      })

      

      //getting user data
      this.fb.api('/me?fields=name', params)
      .then(user => {
        user.picture = `https://graph.facebook.com/${userId}/picture?type=large`;
        this.nativeStorage.setItem('user', {
          name: user.name,
          picture: user.picture,
          userId: userId
        })
        .then(() => {
          this.navCtrl.setRoot(HomePage);
        })
        .catch(err => alert('COULD NOT SET ITEM ' + JSON.stringify(err)))
      })
      .catch(err => alert('COULD NOT GET USER DATA' + JSON.stringify(err)))      
    })
    .catch(err => alert('COULD NOT LOGIN' + JSON.stringify(err)))      
  }

}
