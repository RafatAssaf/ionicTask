import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Listing } from '../../models/listing.model';
import { HomePage } from '../home/home';

import { FirebaseProvider } from '../../providers/firebase/firebase';

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
export class FormPage {

  formType: boolean;

  listing: Listing = {
    category: '',
    quantity: 0,
    unit: '',
    priceMax: 0,
    priceMin: 0,
    user: 'rafat'
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fbProvider: FirebaseProvider
  ) {
    this.formType = this.navParams.get('type');
    this.listing.user = this.navParams.get('userId')
  }

  isValidListing(listing: Listing) {
    return Object.keys(listing).reduce((acc, key) => {
      return acc && (listing[key] !== undefined)
    }, true)
  }

  onSubmit() {
    if(this.isValidListing(this.listing)){
      alert('form submitted!' + JSON.stringify(this.listing));
    } else {
      alert('Please provide all the informations.')
    }
  }

  addListing(listing: Listing, formType: boolean = this.formType) {
    this.fbProvider.addListing(listing, formType? 'buy': 'sell').then((ref) => {
      alert(ref.key)
    })
    this.navCtrl.setRoot(HomePage);
  }

}
