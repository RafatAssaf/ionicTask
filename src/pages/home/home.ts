import { Component } from '@angular/core';
import { NavController, Events, NavParams, ActionSheetController } from 'ionic-angular';

import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';

import { LoginPage } from '../login/login';

import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';
import { Listing } from '../../models/listing.model';
import { Profile } from '../../models/profile.model';
 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  //TODO: make a "user" interface 
  user: Profile = {
    name: 'Ahmad',
    userId: "1234567890",
    picture: "https://cdn.iconscout.com/public/images/icon/free/png-512/avatar-user-teacher-312a499a08079a12-512x512.png"
  };
  userIsReady: boolean = false;
  tab = 'buy';

  buyListings$: Observable<Listing[]>
  sellListings$: Observable<Listing[]>

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: Facebook,
    public nativeStorage: NativeStorage,
    public events: Events,
    public firebaseProvider: FirebaseProvider,
    public actionSheetCtrl: ActionSheetController
  ) {
    events.subscribe('user:logout', () => this.logout())

    this.buyListings$ = this.firebaseProvider
    .getBuyListings()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      })).filter(listing => listing.user === this.user.userId)
    })

    this.sellListings$ = this.firebaseProvider
    .getSellListings()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      })).filter(listing => listing.user === this.user.userId)
    })

    this.nativeStorage.getItem('user')
    .then(data => {
      this.user = {
        name: data.name,
        picture: data.picture,
        userId: data.userId
      };
      this.userIsReady = true;

      //add user profile to firebase
      this.firebaseProvider.addUserProfile(this.user);
    })
    .catch(err => alert('COULD NOT GET USER FROM HOMEPAGE' + JSON.stringify(err)))
  }

  test() {
    
  }

  toForm = (type) => {
    this.navCtrl.push('FormPage', {type: type, userId: this.user.userId});
  }

  logout() {
    this.fb.logout()
    .then(res => {
      this.nativeStorage.remove('user').then(() => {
        this.navCtrl.setRoot(LoginPage);
      });
    })
    .catch(err => {console.log(err)})    
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'ADD NEW LISTING',
      buttons: [
        {
          text: 'New Buy Listing',
          icon: 'add',
          handler: () => {
            this.toForm(true);
          }
        },{
          text: 'New Sell Listing',
          icon: 'remove',
          handler: () => {
            this.toForm(false);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          icon: 'back',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  onCardClicked(listing) {
    this.navCtrl.push("ListingDetailsPage", {listing: listing});
  }

}
