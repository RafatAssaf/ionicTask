import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';
import { Listing } from '../../models/listing.model';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';


@IonicPage()
@Component({
  selector: 'page-listing-details',
  templateUrl: 'listing-details.html',
})
export class ListingDetailsPage {

  listing;
  demands$: Observable<Listing[]>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseProvider: FirebaseProvider,
    public actionSheetCtrl: ActionSheetController
  ) {
    
    this.listing = this.navParams.get('listing');

    this.demands$ = this.firebaseProvider
    .getBuyListings()
    .snapshotChanges()
    .map(changes => {
      return changes.map(c => ({
        key: c.payload.key,
        ...c.payload.val()
      })).filter(listing => (listing.user !== this.listing.user) && (listing.category === this.listing.category))
    })
  }

  onDemandCardClick(customerId, thisUserId = this.listing.user) {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select This Offer",
      buttons: [
        {
          text: 'Contact Customer',
          icon: 'chatbubbles',
          handler: () => {
            this.navCtrl.push('ChatPage', {fromUserId: thisUserId, toUserId: customerId})
          }
        },
        {
          text: 'Accept after Editing (not working yet)',
          icon: 'options',
          handler: () => {alert('Let\'s negotiate!')}
        },
        {
          text: "Accept Offer (not working yet)",
          icon: 'checkmark-circle',
          handler: () => {alert('look\'s good!')}
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'arrow-back',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }
  
  
}
