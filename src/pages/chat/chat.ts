import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

import { FirebaseProvider } from '../../providers/firebase/firebase';

import { Profile } from '../../models/profile.model';
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { Chat } from '../../models/chat.model';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';


@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  toUserProfile: Observable<{}>;
  fromUserProfile: Observable<{}>;
  chat: AngularFireObject<{}>
  msgContent: string;
  messages: Message[];

  //for testing
  toId =  '1234567890';
  fromId = '1930900917227294';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private firebaseProvider: FirebaseProvider
  ) {
    
    this.fromUserProfile = this.firebaseProvider
    .getUserProfile(this.fromId);
    
    this.toUserProfile = this.firebaseProvider
    .getUserProfile(this.toId);
    
    // this.navParams.get('toUserId')
    
    //creating chat record
    this.firebaseProvider.addChat(this.toId, this.fromId);
    
    //assign chat observable
    this.chat = this.firebaseProvider.getChat(this.toId, this.fromId);

    //get messages
    this.chat.snapshotChanges().subscribe(action => {
      this.messages = action.payload.val().messages;
    })
  }

  sendMessage() {
    this.chat.update({
      messages: [
        ...this.messages,
        {
          body: this.msgContent,
          owner: this.fromId
        }
      ]
    })
    this.msgContent = "";
  }

}
