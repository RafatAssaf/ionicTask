import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';


import { Listing } from '../../models/listing.model';
import { Chat } from '../../models/chat.model';
import { Message } from '../../models/message.model';
import { Profile } from '../../models/profile.model';

@Injectable()
export class FirebaseProvider {

  private buyListingsRef = this._database.list<Listing>('/buy-listings');
  private sellListingsRef = this._database.list<Listing>('/sell-listings');  
  private chatsRef = this._database.list<Chat>('/chats');
  private profilesRef = this._database.list<Profile>('/profiles');

  constructor(
    private _database: AngularFireDatabase,
  ) {}

  getBuyListings() {
    return this.buyListingsRef;
  }

  getSellListings() {
    return this.sellListingsRef;
  }

  addListing(listing: Listing, type: string) {
    return type === 'buy'
    ? this.buyListingsRef.push(listing)
    : this.sellListingsRef.push(listing)
  }

  addChat(customerId, thisUserId) {
    //first check if chat exists
    if(this.getChat(customerId, thisUserId).valueChanges() === null) {
      //if not, add it
      const newChat = this._database.object(`/chats/${customerId + '-' + thisUserId}`);
      newChat.set({
        contacts:[customerId, thisUserId], 
        messages: [{
          body: 'conversation start',
          owner:'0'
        }]
      })
    }
  }

  getChat(customerId, thisUserId) {
    return this._database.object(`/chats/${customerId + '-' + thisUserId}`);
  }

  addUserProfile(userProfile: Profile) {
    const newProfile = this._database.object(`/profiles/${userProfile.userId}`);
    newProfile.set(userProfile);
  }

  getUserProfile(userId) {
    return this._database.object(`/profiles/${userId}`).valueChanges();
  }

}
